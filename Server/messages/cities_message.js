var db = require('../database')

//////////////////////////////////// cities ///////////////////////////////////

exports.postCities = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }

        data.query(`SELECT * FROM cities where state_id=('${req.state_id}') and COD_price=('${req.COD_price}') and city_name=('${req.city_name}')`, function (erro, resu) {
            if (Object.keys(resu).length == 0) {
                db.connect(connectionData, function (err, da) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    da.query(`INSERT INTO cities SET?`, req, function (error, result) {
                        callback(error, result);
                    })
                })
            }
            else {
                callback(erro, { find: 'this cities is inserted previos' });

            }

        })
    })

}
exports.getCities = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT city_name , COD_price FROM cities where state_id=('${req.to_id}')`, function (error, result) {
            callback(error, result)
        })
    })
}
exports.getCitiesAdmin = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }

        data.query(`SELECT 
        cities.id ,
        cities.city_name ,
        cities.COD_price , 
        states.location 
        FROM cities JOIN states ON cities.state_id=states.id where cities.state_id=('${req.state_id}')`, function (error, result) {
                callback(error, result)
            })
    })
}

exports.getAllCities = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM cities`, function (error, result) {
            callback(error, result)
        })
    })
}
exports.getSpecificCityById = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT 
        cities.id ,
        cities.city_name ,
        cities.COD_price , 
        cities.state_id,
        states.location 
        FROM cities JOIN states ON cities.state_id=states.id where cities.id=('${req.id}')`, function (error, result) {
                callback(error, result)
            })
    })
}
exports.deleteCity = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`DELETE FROM cities where id=('${req.id}')`, function (error, result) {
            callback(error, result)
        })
    })
}
exports.updateCity = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`UPDATE cities SET COD_price=('${req.COD_price}') , city_name=('${req.city_name}') 
         WHERE id=('${req.id}')`, function (error, result) {
                callback(error, result)
            })
    })
}
