
var db = require('../database')

/////////////////////////////// Categories ////////////////////////////////////////////////////

exports.postCategory = function(connectionData , req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM order_categories where category=('${req.category}')`, req , function(error , result){
            if(Object.keys(result).length==0){
                db.connect(connectionData , function(err , data){
                    if(err){
                        callback(err);
                        return;
                    }
                    data.query(`INSERT INTO order_categories SET?`, req , function(erro , resu){
                        callback(erro , resu)
                    })
                })
            }
            else{
                callback(error , {find :'this category is inserted previos'});

            }
        })
    })
}
exports.getCategories = function(connectionData , callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM order_categories` , function(error , result){
            callback(error , result)
        })  
    })
}

exports.getCategoriesById = function(connectionData ,req , callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM order_categories WHERE id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}
exports.deleteCategory = function(connectionData , req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`DELETE FROM order_categories where id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}
exports.updateCategory = function(connectionData , req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        
        data.query( `UPDATE order_categories SET category=('${req.category}') WHERE id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}

