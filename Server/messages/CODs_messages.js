

var db = require('../database')


///////////////////////////////////   Cod's /////////////////////////////////////
exports.postCod = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM cod where from_id=('${req.from_id}') and to_id=('${req.to_id}')`, req, function (error, result) {
            if (Object.keys(result).length == 0) {
                db.connect(connectionData, function (err, data) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    data.query(`INSERT INTO cod SET?`, req, function (erro, resu) {
                        callback(erro, resu)
                    })
                })
            }
            else {
                callback(error, { find: 'this cities is inserted previos' });

            }
        })
    })

}
exports.getCOD = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        console.log(req)
        const query=`SELECT * FROM cod where from_id=('${req.from_id}') and to_id=('${req.to_id}')`
        console.log(query)
        data.query(query, function (error, result) {
            callback(error, result)
        })
    })
}
// using map table to get keys 
exports.getAllCOD = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`SELECT * FROM states`, function (err, result) {
            var map = { "states": [] };
            map1 = {}
            const length = Object.keys(result).length;
            for (i = 0; i < length; i++) {
                key = result[i].id
                map1[key] = result[i].location
                map['states'].push({ state_id: result[i].id, location: result[i].location })
            }
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return;
                }
                data.query(`SELECT * FROM cod`, function (error, result) {
                    map2 = []

                    length2 = Object.keys(result).length;
                    for (i = 0; i < length2; i++) {
                        const from_location = map1[result[i].from_id]
                        const to_location = map1[result[i].to_id]
                        map2.push({
                            id: result[i].id, from_id: result[i].from_id, to_id: result[i].to_id,
                            from_loc: from_location, to_loc: to_location, COD_price: result[i].COD_price
                        })
                    }
                    map['cod'] = map2;
                    callback(error, map)
                })
            })
        })
    })
}
exports.getAllCOD2 = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`SELECT * FROM states`, function (err, result) {
            var map = {};
            keys = []
            const length = Object.keys(result).length;
            for (i = 0; i < length; i++) {
                key = result[i].id
                map[key] = result[i].location
                keys.push(key)
            }
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return;
                }
                data.query(`SELECT * FROM cod`, function (error, result) {

                    map2 = {}
                    length2 = Object.keys(result).length;

                    for (i = 0; i < length2; i++) {
                        const from_location = map[result[i].from_id]
                        const to_location = map[result[i].to_id]
                        if (map2[result[i].from_id]) {
                            map2[result[i].from_id].push({
                                id: result[i].id, from_id: result[i].from_id, to_id: result[i].to_id,
                                from_loc: from_location, to_loc: to_location, COD_price: result[i].COD_price
                            })
                        }
                        else {
                            map2[result[i].from_id] = [{
                                id: result[i].id, from_id: result[i].from_id, to_id: result[i].to_id,
                                from_loc: from_location, to_loc: to_location, COD_price: result[i].COD_price
                            }]
                        }

                    }
                    map3 = [];

                    for (j = 0; j < keys.length; j++) {
                        map3.push({
                            'state_id': keys[j],
                            'state': map[keys[j]],
                            'cod': map2[keys[j]]
                        })
                    }

                    callback(error, map3)
                })
            })
        })
    })
}
exports.getCodForState = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM states`, function (err, result) {
            var map = {};
            const length = Object.keys(result).length;
            for (i = 0; i < length; i++) {
                var key = result[i].id;
                map[key] = result[i].location
            }
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return;
                }
                data.query(`SELECT * FROM cod where from_id=('${req.id}') or to_id=('${req.id}') `, function (error, result) {
                    map2 = {}
                    length2 = Object.keys(result).length;
                    for (i = 0; i < length2; i++) {
                        const from_location = map[result[i].from_id]
                        const to_location = map[result[i].to_id]
                        if (map2[result[i].from_id, from_location]) {
                            map2[result[i].from_id, from_location].push({
                                id: result[i].id, from_id: result[i].from_id, to_id: result[i].to_id,
                                from_loc: from_location, to_loc: to_location, COD_price: result[i].COD_price
                            })
                        }
                        else {
                            map2[result[i].from_id, from_location] = [{
                                id: result[i].id, from_id: result[i].from_id, to_id: result[i].to_id,
                                from_loc: from_location, to_loc: to_location, COD_price: result[i].COD_price
                            }]
                        }

                    }
                    callback(error, map2)
                })
            })
        })
    })
}
exports.deleteCOD = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`DELETE FROM cod where id=('${req.id}')`, function (error, result) {
            callback(error, result)
        })
    })
}
exports.updateCOD = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`UPDATE cod SET from_id=('${req.from_id}'), to_id=('${req.to_id}') , COD_price=('${req.COD_price}') 
         WHERE id=('${req.id}')`, function (error, result) {
                callback(error, result)
            })
    })
}

