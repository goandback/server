
var db = require('../database')
//////////////////////////////////////// Orders ///////////////////////////////////////////////////////////
exports.InsertBill = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return
        }
        qu = '';
        console.log(req.date);
        date = new Date(req.date)
        console.log(date)
        if (req.driver_id) {
            qu = `INSERT INTO bill_order (reciver_name,From_location,to_location,street,discription,date,bucket_type,
                mandatory_mobile,optional_mobile,accountedBy,COD,notes ,user_id ,price , driver_id  ,state ,created_date)
                VALUES ('${req.reciver_name}' ,'${req.From_location}' ,'${req.to_location}','${req.street}' ,'${req.discription}',now(),
                '${req.bucket_type}','${req.mandatory_mobile}','${req.optional_mobile}','${req.accountedBy}','${req.COD}',
                '${req.notes}','${req.user_id}','${req.price}' ,'${req.driver_id}'  , '${req.state}' , '${req.created_date}')`
        }
        else {
            qu = `INSERT INTO bill_order (reciver_name,From_location,to_location,street,discription,date,bucket_type,
            mandatory_mobile,optional_mobile,accountedBy,COD,notes ,user_id ,price,state ,created_date)
            VALUES ('${req.reciver_name}' ,'${req.From_location}' ,'${req.to_location}','${req.street}' ,'${req.discription}',now(),
            '${req.bucket_type}','${req.mandatory_mobile}','${req.optional_mobile}','${req.accountedBy}','${req.COD}',
            '${req.notes}','${req.user_id}','${req.price}'  , '${req.state}' , '${req.created_date}') `
        }
        data.query(qu, function (error, result) {
            callback(error, result)
        })
    })
}
exports.InsertBillTrack = function (connectionData, req, callback) {

    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return
        }
        qu = '';
        if (req.user_type == 'driver') {
            qu = `INSERT INTO track_order (bucket_id,user_id,date)
                VALUES ('${req.bucket_id}' ,'${req.user_id}' ,now())`

            data.query(qu, function (error, result) {
                //console.log(error)
                callback(error, result)
            })
        }
    })

}
exports.UpdateBill = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`UPDATE bill_order SET barcode=('${req.barcode}') WHERE id=('${req.id}')`, function (error, result) {
            callback(error, result)
        })
    })
}

exports.GetAllTraderOrders = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return
        }
        data.query(`SELECT * FROM bill_order WHERE user_id=('${req.user_id}') and status!=4 `, function (error, result) {
            callback(error, result)
        })

    })
}
exports.getAllBillingWithoutDriver = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return

        }


        data.query(`SELECT * FROM user JOIN trader_details ON user.user_id= trader_details.user_id WHERE user.user_type='trader'`, function (error, result) {
            map = {}
            result.map((user) => {
                // console.log(user)
                map[user.user_id] = {
                    user_Fname: user.user_Fname, user_Lname: user.user_Lname,
                    address1: user.address1, address2: user.address2, city: user.city,
                    town: user.town, user_phone1: user.user_phone1, user_phone2: user.user_phone2,
                    logo: user.logo, nick: user.nick
                }

            })
            map2 = {}
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return
                }
                data.query(`SELECT * FROM user WHERE user_type='driver' `, function (error, resul) {
                    resul.map((user) => {
                        map2[user.user_id] = {
                            user_Fname: user.user_Fname, user_Lname: user.user_Lname,
                            address1: user.address1, address2: user.address2, city: user.city,
                            town: user.town, user_phone1: user.user_phone1, user_phone2: user.user_phone2,
                        }
                    })

                    db.connect(connectionData, function (err, data) {
                        if (err) {
                            callback(err);
                            return
                        }
                        data.query(`SELECT * FROM bill_order WHERE status=0 `, function (error, resu) {
                            map3 = []
                            resu.map((order) => {
                                date = new Date(order.created_date);
                                date2 = new Date()
                                //console.log(order.created_date)
                                //console.log(Math.abs((date.getTime() - date2.getTime()) / 3600000))
                                isOver48 = (Math.abs((date.getTime() - date2.getTime()) / 3600000)) >= 48
                                test = {
                                    id: order.id, reciver_name: order.reciver_name, From_location: order.From_location, to_location: order.to_location, street: order.street,
                                    state: order.state, discription: order.discription, date: order.date, bucket_type: order.bucket_type, mandatory_mobile: order.mandatory_mobile,
                                    optional_mobile: order.optional_mobile, accountedBy: order.accountedBy, COD: order.COD,
                                    notes: order.notes,
                                    trader_id: order.user_id,
                                    trader_Info: map[order.user_id],
                                    driver_Info: map2[order.driver_id],
                                    driver_id: order.driver_id,
                                    status: order.status,
                                    price: order.price,
                                    barcode: order.barcode,
                                    isOver48: isOver48,
                                }
                                map3.push(test)
                            })

                            callback(error, map3);

                        })

                    })

                })
            })

        })
    })
}
exports.getOrderWithbarcode = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM bill_order WHERE id=('${req.id}')`, function (err, result) {
            callback(err, result);
        })
    })
}
exports.updateOrder_driver = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `UPDATE bill_order SET driver_id=('${req.user_id}') WHERE id=('${req.order_id}')`;

        data.query(query, function (err, result) {
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return;
                }
                qu = `INSERT INTO track_order (bucket_id,user_id,date)
                VALUES ('${req.order_id}' ,'${req.user_id}' ,now())`

                data.query(qu, function (error, result) {
                    callback(err, { success: true })
                })
            })
        })

    })
}

exports.getTraderId = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM bill_order WHERE id=('${req.order_id}')`, function (err, result) {
            db.connect(connectionData, function (err, data) {
                if (err) {
                    callback(err);
                    return;
                }
                console.log(result[0])
                data.query(`SELECT * FROM user WHERE user_id=('${result[0].user_id}')`, function (err, result) {
                    callback(err, result)
                })

            })
        })
    })
}
exports.getAllOrderForDriverNotFinish = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return
        }
        data.query(`SELECT * FROM bill_order WHERE driver_id=('${req.user_id}') and status!=4 `, function (error, result) {
            callback(error, result);
        })
    })
}

exports.GetAllOrderTracking = function (connectionData, req, callback) {
    const qu = `SELECT 
bill_order.id,
bill_order.reciver_name,
bill_order.From_location, 
bill_order.to_location,
bill_order.street, 
bill_order.state,
bill_order.discription,
bill_order.date,
bill_order.bucket_type,
bill_order.mandatory_mobile,
bill_order.optional_mobile,
bill_order.accountedBy,
bill_order.COD,
bill_order.notes, 
bill_order.user_id, 
bill_order.driver_id,
bill_order.status,
bill_order.price,
bill_order.barcode ,
track_order.user_id as driver_id,
track_order.id as track_id,
track_order.date ,
user.user_Fname as driver_Fname,
user.user_Lname as driver_Lname,
user.user_phone1 as driver_phone1,
user.user_phone2 as driver_phone2
FROM bill_order JOIN track_order ON bill_order.id=track_order.bucket_id JOIN user ON track_order.user_id=user.user_id
WHERE bill_order.id=('${req.id}')`


    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(qu, function (err, result) {
            if (Object.keys(result).length > 0) {
                db.connect(connectionData, function (err, data1) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    qu2 = `SELECT bill_order.user_id , user.user_Fname , user.user_Lname , bill_order.created_date ,user.user_type  from bill_order JOIN user on bill_order.user_id=user.user_id where bill_order.id='${req.id}' limit 1`
                    data1.query(qu2, function (err, result1) {
                        var trader_Info = result1[0]
                        console.log(result1[0])
                        if (result1[0].user_type === 'trader') {
                            db.connect(connectionData, function (err, data2) {
                                if (err) {
                                    callback(err);
                                    return;
                                }
                                const user_id = result1[0].user_id
                                qu3 = `SELECT * from trader_details where user_id='${user_id}'`
                                data2.query(qu3, function (err, result3) {
                                    console.log(result3[0].nick)
                                    map = []
                                    Object.keys(result).map(i => {
                                        console.log(i)
                                        item = { ...result[i] }
                                        item['trader_Fname'] = trader_Info.user_Fname
                                        item['trader_Lname'] = trader_Info.user_Lname
                                        item['created_date'] = trader_Info.created_date
                                        item['user_type'] = trader_Info.user_type
                                        item['nick'] = result3[0].nick
                                        map.push(item)
                                    })
                                    callback(err, map)
                                })
                            })
                        }
                        else {
                            map = []
                            for (i = 0; i < Object.keys(result).length; i++) {
                                item = { ...result[i] }
                                item['trader_Fname'] = trader_Info.user_Fname
                                item['trader_Lname'] = trader_Info.user_Lname
                                item['created_date'] = trader_Info.created_date
                                item['user_type'] = trader_Info.user_type

                                map.push(item)
                            }
                            callback(err, map)
                        }

                    })
                })
            }
            else {
                callback(err, [])

            }
        })
    })
}
exports.updateOrderDriverFromAdmin = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        var query = ''
        if (req.driver_id != 0) {
            query = `UPDATE bill_order SET driver_id=('${req.driver_id}') WHERE id=('${req.id}')`
        }
        else {
            query = `UPDATE bill_order SET driver_id=null WHERE id=('${req.id}')`
        }

        data.query(query, function (err, result) {
            callback(err, result)
        })
    })

}

exports.GetNumberOfNewOrder = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = ` SELECT COUNT( * ) as "NumberOfNewOrder" FROM  bill_order WHERE status='0'`
        data.query(query, function (err, result) {
            console.log(result)
            callback(err, result)
        })
    })
}


exports.GetNumberOfNewOrderWithDriver = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `SELECT 
                bill_order.id,
                bill_order.reciver_name,
                bill_order.From_location, 
                bill_order.to_location,
                bill_order.street, 
                bill_order.state,
                bill_order.discription,
                bill_order.date,
                bill_order.bucket_type,
                bill_order.mandatory_mobile,
                bill_order.optional_mobile,
                bill_order.accountedBy,
                bill_order.COD,
                bill_order.notes, 
                bill_order.user_id, 
                bill_order.driver_id,
                bill_order.status,
                bill_order.price,
                bill_order.barcode ,
                trader_details.logo as trader_logo,
                trader_details.nick as trader_nickName,
                u2.user_Fname as trader_Fname,
                u2.user_Lname as trader_Lname,
                u2.user_phone1 as trader_phone1,
                u2.user_phone2 as trader_phone2
                FROM bill_order 
                JOIN user as u2 ON bill_order.user_id=u2.user_id
                LEFT JOIN trader_details ON u2.user_id=trader_details.user_id AND u2.user_type='trader'
                WHERE bill_order.status='1' and bill_order.driver_id is NOT null`
        data.query(query, function (err, result) {

            callback(err, result)
        })
    })
}

exports.GetAllDeliveredOrders = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `SELECT 
                bill_order.id,
                bill_order.reciver_name,
                bill_order.From_location, 
                bill_order.to_location,
                bill_order.street, 
                bill_order.state,
                bill_order.discription,
                bill_order.date,
                bill_order.bucket_type,
                bill_order.mandatory_mobile,
                bill_order.optional_mobile,
                bill_order.accountedBy,
                bill_order.COD,
                bill_order.notes, 
                bill_order.user_id, 
                bill_order.driver_id,
                bill_order.status,
                bill_order.price,
                bill_order.barcode ,
                track_order.user_id as driver_id,
                track_order.date ,
                trader_details.logo as trader_logo,
                trader_details.nick as trader_nickName,
                user.user_Fname as driver_Fname,
                user.user_Lname as driver_Lname,
                user.user_phone1 as driver_phone1,
                user.user_phone2 as driver_phone2,
                u2.user_Fname as trader_Fname,
                u2.user_Lname as trader_Lname,
                u2.user_phone1 as trader_phone1,
                u2.user_phone2 as trader_phone2
                FROM bill_order 
                JOIN track_order ON bill_order.id=track_order.bucket_id 
                JOIN user ON track_order.user_id=user.user_id
                JOIN user as u2 ON bill_order.user_id=u2.user_id
                LEFT JOIN trader_details ON u2.user_id=trader_details.user_id AND u2.user_type='trader'
                WHERE bill_order.status='4' `

        data.query(query, function (err, result) {
            map = {}
            result.map(res => {
                console.log(res.id)
                if (map[res.id]) {
                    map[res.id].push(res)
                    console.log(map)
                }
                else {
                    map[res.id] = [res]
                }
            })
            callback(err, map)
        })
    })
}

exports.GetOrderInDate = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `SELECT 
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.user_id, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.barcode 
            FROM bill_order 
            WHERE bill_order.date between '${req.date}' and '${req.date} 23:59:59'`

        //` SELECT * FROM  bill_order WHERE date BETWEEN ('${date}') and ('${date2}')  `

        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

exports.GetOrderInDateFromTo = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }

        const query = `SELECT 
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.user_id, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.barcode 
            FROM bill_order 
            WHERE bill_order.date between '${req.fromDate}' and '${req.toDate} 23:59:59'`

        //` SELECT * FROM  bill_order WHERE date BETWEEN ('${date}') and ('${date2}')  `

        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

exports.GetTraderOrderInDateFromTo= function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `SELECT 
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.user_id, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.barcode  
            FROM bill_order 
            WHERE bill_order.user_id='${req.id}' and bill_order.date between '${req.fromDate}' and '${req.toDate} 23:59:59' `

        //` SELECT * FROM  bill_order WHERE date BETWEEN ('${date}') and ('${date2}')  `

        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

exports.GetDriverOrderInDateFromTo = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }

        const query = `SELECT 
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.user_id, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.barcode 
            FROM bill_order JOIN track_order ON bill_order.id=track_order.bucket_id 
            WHERE track_order.date between '${req.fromDate}' and '${req.toDate} 23:59:59' and track_order.user_id='${req.id}'`

        //` SELECT * FROM  bill_order WHERE date BETWEEN ('${date}') and ('${date2}')  `

        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

exports.getAllDriver = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = ` SELECT * FROM user JOIN driver_details ON driver_details.user_id=user.user_id WHERE user_type='driver'`
        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

exports.getAllTrader = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = ` SELECT * FROM user JOIN trader_details ON trader_details.user_id=user.user_id WHERE user_type='trader'`
        data.query(query, function (err, result) {
            callback(err, result)
        })
    })
}

////////////////////////////////////////////////////////////////////////////////////////////////////


exports.GetBackedOrder = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        const query = `SELECT 
                bill_order.id,
                bill_order.reciver_name,
                bill_order.From_location, 
                bill_order.to_location,
                bill_order.street, 
                bill_order.state,
                bill_order.discription,
                bill_order.date,
                bill_order.bucket_type,
                bill_order.mandatory_mobile,
                bill_order.optional_mobile,
                bill_order.accountedBy,
                bill_order.COD,
                bill_order.notes, 
                bill_order.user_id, 
                bill_order.driver_id,
                bill_order.status,
                bill_order.price,
                bill_order.barcode ,
                track_order.user_id as driver_id,
                track_order.date ,
                trader_details.logo as trader_logo,
                trader_details.nick as trader_nickName,
                user.user_Fname as driver_Fname,
                user.user_Lname as driver_Lname,
                user.user_phone1 as driver_phone1,
                user.user_phone2 as driver_phone2,
                u2.user_Fname as trader_Fname,
                u2.user_Lname as trader_Lname,
                u2.user_phone1 as trader_phone1,
                u2.user_phone2 as trader_phone2
                FROM bill_order 
                JOIN track_order ON bill_order.id=track_order.bucket_id 
                JOIN user ON track_order.user_id=user.user_id
                JOIN user as u2 ON bill_order.user_id=u2.user_id
                LEFT JOIN trader_details ON u2.user_id=trader_details.user_id AND u2.user_type='trader'
                WHERE bill_order.status='3' `

        data.query(query, function (err, result) {
            map = {}
            result.map(res => {
                console.log(res.id)
                if (map[res.id]) {
                    map[res.id].push(res)
                    console.log(map)
                }
                else {
                    map[res.id] = [res]
                }
            })
            callback(err, map)
        })
    })
}
exports.getorderDuringLast48 = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        date2 = new Date()

        const query = `SELECT * FROM (SELECT 
            bill_order.user_id as uu,
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.created_date
            from bill_order join user on bill_order.user_id=user.user_id) as TT join trader_details on trader_details.user_id=TT.uu where TT.status=0 and TT.driver_id is null`
        map = []

        data.query(query, function (err, result) {
            Object.keys(result).map(i => {
                date = new Date(result[i].created_date);
                if ((Math.abs((date.getTime() - date2.getTime()) / 3600000)) >= 48) {
                    map.push(result[i])
                }
            })

            callback(err, map)
        })

    })

}



exports.getorderSubmitDuringLast48 = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        date2 = new Date()

        const query = `SELECT * FROM (SELECT 
            bill_order.user_id as uu,
            bill_order.id,
            bill_order.reciver_name,
            bill_order.From_location, 
            bill_order.to_location,
            bill_order.street, 
            bill_order.state,
            bill_order.discription,
            bill_order.date,
            bill_order.bucket_type,
            bill_order.mandatory_mobile,
            bill_order.optional_mobile,
            bill_order.accountedBy,
            bill_order.COD,
            bill_order.notes, 
            bill_order.driver_id,
            bill_order.status,
            bill_order.price,
            bill_order.created_date,
            bill_order.updated_date
            from bill_order join user on bill_order.user_id=user.user_id) as TT join trader_details on trader_details.user_id=TT.uu where TT.status='4' `
        map = []

        data.query(query, function (err, result) {
            Object.keys(result).map(i => {
                date = new Date(result[i].updated_date);
                console.log(date)
                if ((Math.abs((date.getTime() - date2.getTime()) / 3600000)) >= 48) {
                    map.push(result[i])
                }
            })

            callback(err, map)
        })

    })

}