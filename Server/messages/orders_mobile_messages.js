var db = require('../database')
var admin = require('firebase-admin');

var serviceAccount = require('../goandback-d4256-firebase-adminsdk-4p7qc-9de7d7f147.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://goandback-d4256.firebaseio.com"
})



exports.updateBillStatus = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        var qu = '';
        if (req.reason) {
            qu = `UPDATE bill_order SET status=('${req.status}') , reason_return=('${req.reason}') , updated_date=(now()) WHERE id=('${req.id}')`

        }
        else {
            qu = `UPDATE bill_order SET status=('${req.status}'), updated_date=(now()) WHERE id=('${req.id}')`
        }
        console.log(qu)
        data.query(qu, function (error, result) {
            db.connect(connectionData, function (err, data) {
                data.query(`SELECT user.token_not from bill_order JOIN user on 
                bill_order.user_id=user.user_id WHERE bill_order.id='${req.id}'`, function (error, result2) {
                    token = result2[0].token_not
                    var registrationToken = [token]
                    console.log(registrationToken)

                    var payload = {
                        notification: {
                            title: "Account Deposit1",
                            body: "A deposit to your savings account has just cleared.",
                            sound: "default"
                        },
                        data: {
                            account: "Savings",
                            balance: "$3020.25"
                        }
                    };
                    var options = {
                        priority: "high",
                        timeToLive: 60 * 60 * 24
                    };


                    admin.messaging().sendToDevice(registrationToken, payload, options)
                        .then(function (response) {
                            console.log("Successfully sent message:", response);
                            console.log(response.results[0].error)
                        })
                        .catch(function (error) {
                            console.log("Error sending message:", error);
                        });

                    callback(error, result)

                })

            })
        })
    })
}

exports.getDeliveredOrderFoTrader = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order  WHERE status='4' and user_id='${req.id}'`, function (err, result) {
            callback(err, result)
        })
    })
}



exports.getBackedOrderFoTrader = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order  WHERE status='2' and user_id='${req.id}'`, function (err, result) {
            callback(err, result)
        })
    })
}




exports.getpendingOrderFoTrader = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order  WHERE status='3' and user_id='${req.id}'`, function (err, result) {
            callback(err, result)
        })
    })
}

exports.getLastmonthDeliverdOrderByDriver = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order WHERE created_date >= (SELECT CURRENT_TIME - INTERVAL 30 DAY) and created_date <= now()
        and driver_id='${req.id}' and status='4'`, function (err, result) {
            console.log(err)
            callback(err, result)
        })
    })
}

exports.getLastmonthBackedOrderByDriver= function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order WHERE created_date >= (SELECT CURRENT_TIME - INTERVAL 30 DAY) and created_date <= now()
        and driver_id='${req.id}' and status='2'`, function (err, result) {
            console.log(err)
            callback(err, result)
        })
    })
}
exports.getLastmonthStuckOrderByDriver =  function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * from bill_order WHERE created_date >= (SELECT CURRENT_TIME - INTERVAL 30 DAY) and created_date <= now()
        and driver_id='${req.id}' and status='3'`, function (err, result) {
            console.log(err)
            callback(err, result)
        })
    })
}