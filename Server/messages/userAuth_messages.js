var db = require('../database')

//////////////////////////////////////////Sign In /////////////////////////////////////////////////////////
exports.SignIn = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM user WHERE user_email=('${req.user_email}') and user_password=('${req.user_password}')`, function (err, result) {
            //console.log(result)
            callback(err, result)
        })
    })
}
/////////////////////////////////////////////////////////////////////////////////////////////

exports.UpdateUserToken = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        data.query(`UPDATE user SET token=('${req.token}') , edit_date=now() 
            where user_id=('${req.user_id}')`, function (error, result) {
                callback(error, result)
            })

    })
}

exports.updateNotToken = function (connectionData, req, callback) {

    db.connect(connectionData, function (err, data) {
        console.log(err)
        data.query(`UPDATE user SET token_not=('${req.token_not}') , edit_date=now() 
        where user_id=('${req.user_id}')`, function (error, result) {
                callback(error, result)
            })

    })
}
exports.getAdminToken = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        //   console.log(err)
        data.query(`SELECT * FROM user WHERE user_type='admin'`, function (error, result) {
            callback(error, result)
        })

    })
}

exports.DeleteUser = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`SELECT user_type from user where user_id=('${req.user_id}')`, function (error, user_type) {
            db.connect(connectionData, function (err, data) {
                data.query(`DELETE FROM user WHERE user_id=('${req.user_id}')`, function (err, result) {
                    if (Object.keys(user_type).length !== 0) {

                        if (user_type[0].user_type == 'trader') {
                            db.connect(connectionData, function (err, data) {
                                data.query(`DELETE FROM trader_details WHERE user_id=('${req.user_id}')`, function (err, resu) {
                                    callback(err, resu)
                                })
                            })
                        }
                        else if (user_type[0].user_type == 'driver') {
                            data.query(`DELETE FROM driver_details WHERE user_id=('${req.user_id}')`, function (err, resu) {
                                callback(err, resu)
                            })
                        } else {
                            callback(err, result)
                        }
                    }
                    else {
                        callback(err, result)
                    }
                })
            })

        })
    })
}
exports.GetAllUsers = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }

        data.query(`SELECT user_id , user_email  , user_type , user_Fname , user_Lname , address1, address2, city, state, postal_code, user_phone1, user_phone2, isActive  ,created_date, edit_date
         FROM user WHERE 1`, function (err, result) {
                // console.log
                callback(err, result)
            })
    })
}
exports.GetAllUsersNotActive = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }

        data.query(`SELECT user_id , user_email  , user_type , user_Fname , user_Lname , address1, address2, city, state, postal_code, user_phone1, user_phone2, isActive  ,created_date, edit_date
         FROM user WHERE isActive='2'`, function (err, result) {
                callback(err, result)
            })
    })
}
exports.UpdateUser = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        var queryUpdate;
        if (req.user_password == null || req.user_password == '' || req.user_password == 'undefined') {
            queryUpdate = `UPDATE user SET user_Fname=('${req.user_Fname}') ,user_Lname=('${req.user_Lname}') ,
            user_email=('${req.user_email}'),
            address1=('${req.address1}'),address2=('${req.address2}') , city=('${req.city}') ,state=('${req.state}') ,postal_code=('${req.postal_code}') ,user_phone1=('${req.user_phone1}'),
            user_phone2=('${req.user_phone2}'), edit_date=now() , isActive=('${req.isActive}') 
            where user_id=('${req.user_id}')`
        }
        else {
            queryUpdate = `UPDATE user SET user_Fname=('${req.user_Fname}') ,user_Lname=('${req.user_Lname}') ,
            user_email=('${req.user_email}'),user_password=('${req.user_password}'),
            address1=('${req.address1}'),address2=('${req.address2}') , city=('${req.city}') ,state=('${req.state}') ,postal_code=('${req.postal_code}') ,user_phone1=('${req.user_phone1}'),
            user_phone2=('${req.user_phone2}'), edit_date=now() , isActive=('${req.isActive}') 
            where user_id=('${req.user_id}')`
        }
        console.log(req.user_type)
        data.query(queryUpdate, function (err, result) {
            if (req.user_type == 'trader') {
                db.connect(connectionData, function (err, da) {
                    if (err) {
                        callback(err)
                        return
                    }
                    da.query(`UPDATE trader_details SET logo=('${req.logo}') ,nick=('${req.nick}') WHERE user_id=('${req.user_id}')`, function (error, resu) {
                        callback(error, resu)
                    });
                });
            }

            else if (req.user_type == 'driver') {
                db.connect(connectionData, function (err, da) {
                    if (err) {
                        callback(err)
                        return
                    }
                    da.query(`UPDATE driver_details SET line_no=('${req.line_no}') , GeoLocation=('${req.GeoLocation}')
                WHERE user_id=('${req.user_id}')`, function (error, resu) {
                            callback(error, resu)
                        });
                });
            }
            else {
                callback(err, result)
            }
        })
    })
}
exports.updateUserIsActive = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`UPDATE user SET isActive=('${req.isActive}') WHERE user_id=('${req.user_id}')`, function (err, result) {
            callback(err, result);
        })
    })
}


exports.UpdateUserPassword = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`UPDATE user SET user_password=('${req.user_password}') WHERE user_id=('${req.user_id}')`, function (err, result) {
            callback(err, result);
        })
    })
}

exports.GetUserDriver = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`SELECT 
    user.user_id , 
    user.user_email , 
    user.user_type , 
    user.user_Fname , 
    user.user_Lname ,
    user.address1, 
    user.address2, 
    user.city, 
    user.state, 
    user.postal_code, 
    user.user_phone1, 
    user.user_phone2, 
    user.isActive ,
    driver_details.line_no,
    driver_details.GeoLocation
    FROM user JOIN driver_details ON user.user_id=driver_details.user_id
    where user.user_id = ('${req.user_id}')`, function (err, result) {
                callback(err, result)
            })
    })
}
exports.GetUserTrader = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`SELECT 
    user.user_id , 
    user.user_email , 
    user.user_type , 
    user.user_Fname , 
    user.user_Lname ,
    user.address1, 
    user.address2, 
    user.city, 
    user.state, 
    user.postal_code, 
    user.user_phone1, 
    user.user_phone2, 
    user.isActive ,
    trader_details.logo ,
    trader_details.nick ,
    trader_details.trader_id
    FROM user JOIN trader_details ON user.user_id=trader_details.user_id
    where user.user_id = ('${req.user_id}')`, function (err, result) {
                callback(err, result)
            })
    })
}
exports.GetUserType = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return
        }
        data.query(`SELECT user_id , user_email  , user_type , user_Fname , user_Lname , address1, address2, city, state, postal_code, user_phone1, user_phone2, isActive 
        FROM user WHERE user_id=('${req.user_id}')`, function (err, result) {
                callback(err, result)
            })
    })
}
////////////////////////////////// create user ///////////////////////////////////////////////
exports.checkuser = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM user WHERE user_email=('${req.user_email}')`, function (err, result) {
            callback(err, result)
        })
    })
}
exports.createUser = function (connectionData, req, callback) {
    da = {
        user_Fname: req.user_Fname,
        user_Lname: req.user_Lname,
        user_password: req.user_password,
        user_email: req.user_email,
        user_type: req.user_type,
        address1: req.address1,
        address2: req.address2,
        city: req.city,
        state: req.state,
        postal_code: req.postal_code,
        user_phone1: req.user_phone1,
        user_phone2: req.user_phone2,
        created_date: req.created_date,
        edit_date: req.edit_date,
    }
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`INSERT INTO user SET?`, da, function (err, result) {

            if (req.user_type == 'driver') {
                db.connect(connectionData, function (err, dat) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    info = { user_id: result.insertId, line_no: req.line_no, GeoLocation: req.GeoLocation }
                    dat.query(`INSERT INTO driver_details SET?`, info, function (error, resu) {
                        callback(error, resu)
                    })

                })
            }
            else if (req.user_type == 'trader') {
                db.connect(connectionData, function (err, dat) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    info = { user_id: result.insertId, logo: req.logo, nick: req.nick }
                    dat.query(`INSERT INTO trader_details SET?`, info, function (error, resu) {
                        callback(error, resu)
                    })

                })
            }
            else {
                callback(err, result)
            }
        })
    })
}
