var db = require('../database')

////////////////////////////////////// package size  / ////////////////////////////////////////////
exports.postPackage = function(connectionData , req , callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM package_size where size=('${req.size}') and cod=('${req.cod}')`, req , function(error , result){
            if(Object.keys(result).length==0){
                db.connect(connectionData , function(err , data){
                    if(err){
                        callback(err);
                        return;
                    }
                    data.query(`INSERT INTO package_size SET?`, req , function(erro , resu){
                        callback(erro , resu)
                    })
                })
            }
            else{
                callback(error , {find :'this package is inserted previos'});

            }
        })
    })
}
exports.getPackages = function(connectionData , callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM package_size` , function(error , result){
            callback(error , result)
        })  
    })
}


exports.getPackagesById = function(connectionData ,req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`SELECT * FROM package_size WHERE id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}

exports.deletePackage = function(connectionData , req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        data.query(`DELETE FROM package_size where id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}
exports.updatePackage = function(connectionData , req, callback){
    db.connect(connectionData , function(err , data){
        if(err){
            callback(err);
            return;
        }
        
        data.query( `UPDATE package_size SET size=('${req.size}') ,cod=('${req.cod}')  WHERE id=('${req.id}')` , function(error , result){
            callback(error , result)
        })  
    })
}

