var db = require('../database')

//////////////////////states /////////////////////////////////

exports.postState = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err);
            return;
        }
        data.query(`SELECT * FROM states where location=('${req.location}')`, function (erro, resu) {
            if (Object.keys(resu).length == 0) {
                db.connect(connectionData, function (err, da) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    da.query(`INSERT INTO states SET?`, req, function (error, result) {
                        insertId = result.insertId;
                        db.connect(connectionData, function (err, data) {
                            if (err) {
                                callback(err);
                                return;
                            }
                            data.query(`SELECT * FROM states`, req, function (error, result) {
                                result.map((test) => {
                                    db.connect(connectionData, function (err, data) {
                                        codData = {
                                            from_id: test.id,
                                            to_id: insertId,
                                            COD_price: 0
                                        }

                                        data.query(`INSERT INTO cod SET?`, codData, function (error, result) {
                                            if (error) {
                                                callback(error)
                                                return
                                            }
                                        })
                                    })
                                    if (test.id != insertId) {
                                        db.connect(connectionData, function (err, data) {
                                            codData = {
                                                from_id: insertId,
                                                to_id: test.id,
                                                COD_price: 0
                                            }

                                            data.query(`INSERT INTO cod SET?`, codData, function (error, result) {
                                                if (error) {
                                                    callback(error)
                                                    return
                                                }
                                            })
                                        })
                                    }
                                })
                            })
                            callback(error, result);

                        })
                    })
                })

            }
            else {
                callback(erro, { find: 'this location is inserted previos' });
            }

        })


    })
}
exports.getStates = function (connectionData, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`SELECT * FROM states`, function (err, result) {
            callback(err, result)
        })

    })

}
exports.deleteState = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }
        data.query(`DELETE FROM states where id=('${req.id}')`, function (error, result) {
            db.connect(connectionData, function (err, data) {

                data.query(`DELETE FROM cities where state_id=('${req.id}')`, function (error, result) {
                    db.connect(connectionData, function (err, data) {

                        data.query(`DELETE FROM cod where from_id=('${req.id}') or to_id=('${req.id}')`, function (error, result) {
                            callback(error, result)
                        })
                    })
                })
            })
        })
    })
}
exports.updateState = function (connectionData, req, callback) {
    db.connect(connectionData, function (err, data) {
        if (err) {
            callback(err)
            return;
        }

        data.query(`UPDATE states SET location=('${req.location}') WHERE id=('${req.id}')`, function (error, result) {
            callback(error, result)
        })
    })
}
