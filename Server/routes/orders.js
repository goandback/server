const express = require('express')
const router = express.Router()
var message = require('../messages/orders_messages')
var message1=require('../messages/userAuth_messages')
var admin=require('firebase-admin');

const dataBaseInfo = {
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
}

////////////////////////////////////////// Orders /////////////////////////////////////////////////////


//////////trader /////////////////

//post order from trader and  driver

router.post('/api/order', function (req, res) {
    //   console.log(req.body);
    //console.log(req.body[0])

    req.body.map((bill, idx) => {
        date = new Date();
        map22 = {};
        optional_mobile = null;
        if (bill.MobileSecondary) {
            optional_mobile = bill.MobileSecondary
        }
        if (bill.user_type == 'driver') {
            billData = {
                reciver_name: bill.NameOfReceiver, From_location: bill.FromLocation, to_location: bill.ToLocation,
                street: bill.Street, discription: bill.Description, date: date, bucket_type: bill.TypeOfPucket,
                mandatory_mobile: bill.MobilePrimary, optional_mobile: optional_mobile,
                accountedBy: bill.OnWho, COD: bill.COD, notes: bill.Note, state: bill.DetailCityValue,
                created_date: date, edit_date: date, user_id: bill.UserId, price: bill.ItemPrice, driver_id: bill.UserId
            }
        }
        else {
            billData = {
                reciver_name: bill.NameOfReceiver, From_location: bill.FromLocation, to_location: bill.ToLocation,
                street: bill.Street, discription: bill.Description, date: date, bucket_type: bill.TypeOfPucket,
                mandatory_mobile: bill.MobilePrimary, optional_mobile: optional_mobile,
                accountedBy: bill.OnWho, COD: bill.COD, notes: bill.Note, state: bill.DetailCityValue,
                created_date: date, edit_date: date, user_id: bill.UserId, price: bill.ItemPrice
            }
        }
        message.InsertBill(dataBaseInfo, billData, function (err, result) {
            //console.log(result.insertId)
            //barcode=bill.UserId +'-'+ (parseInt(result.insertId)).toString()+'-'+uniqueNumber().toString()
            t1 = 6 - bill.UserId.length
            t2 = 7 - result.insertId.toString().length
            console.log(result.insertId.toString().length)
            barcode = '0'.repeat(t1) + bill.UserId + '-' + '0'.repeat(t2) + (parseInt(result.insertId)).toString()
            console.log(barcode)
            map22[result.insertId] = barcode
            message.UpdateBill(dataBaseInfo, { id: result.insertId, barcode: barcode }, function (err, resul) {
                //console.log(result)
                message.InsertBillTrack(dataBaseInfo, { bucket_id: result.insertId, user_id: bill.UserId, date: new Date(), user_type: bill.user_type }, function (err, resu) {

                })
            })
        })

    })

    message1.getAdminToken(dataBaseInfo, function (err, result) {
        var registrationToken = [];
        registrationToken.push(result[0].token_not);
        console.log(registrationToken);
        var payload = {
            notification: {
                title: "طرود جديدة",
                body: "تم اضافة " + " " + req.body.length + " " + "طرد جديد",
                sound: "default",
            },
            data: {
                NumOfOrders: "" + req.body.length,
            },

        };
        var options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };


        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(function (response) {
                console.log("Successfully sent message:", response);
                console.log(response.results[0].error)
            })
            .catch(function (error) {
                console.log("Error sending message:", error);
            });

    })

    res.status(201)
    res.json([{ "success": true }])
})
//////////////////////////

function uniqueNumber() {
    var date = Date.now();

    if (date <= uniqueNumber.previous) {
        date = ++uniqueNumber.previous;
    } else {
        uniqueNumber.previous = date;
    }

    return date;
}

uniqueNumber.previous = 0;
//order for trader
router.get('/api/order', function (req, res) {


    data = {
        user_id: req.query.IdUser
    }
    console.log(data)
    message.GetAllTraderOrders(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.json({});

        }
        else {
            res.status(201);
            console.log(result)
            res.json({ "orders": result });
        }
    })
})

/////////////////////////////////// Admin /////////////////////////////

//get all order without driver for admin
router.get('/api/AllOrderWithoutDriver', function (req, res) {
    message.getAllBillingWithoutDriver(dataBaseInfo, function (error, result) {
        if (error) {
            res.status(400);
            res.end(error + '');
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})
//get order with barcode xx
router.get('/api/orderWithBarcode', function (req, res) {
    console.log(req.query.barcode)
    str = req.query.barcode
    var val = str.split("-");
    data = {
        id: val[1]
    }
    message.getOrderWithbarcode(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json(result);
        }
    })


})
///////////////////// admin
/*
in this api frontend send id for order and then
return array of object that have all order information with driver info
tracking
*/
router.get('/api/OrderTracking', function (req, res) {

    data = {
        id: req.query.id
    }
    console.log(data)

    message.GetAllOrderTracking(dataBaseInfo, data, function (error, result) {
        if (error) {
            res.status(400);
            res.end(error + "");
        }
        else {
            res.status(201);
            res.json(result);
        }
    })

})

//////////// update driver for a set of orders  
// under test 

router.put('/api/orderDriverChoice', function (req, res) {
    Object.keys(req.body.id).map(id_update => {
        data = {
            id: req.body.id[id_update],
            driver_id: req.body.driver_id
        }

        message.updateOrderDriverFromAdmin(dataBaseInfo, data, function (err, result) {
            if (err) {
                res.status(400)
                res.end(err + '');
            }

        })

    });
    res.status(201)
    res.json({ success: true });
})

//// Number of new order 

router.get('/api/NumberOfNewOrder', function (req, res) {
    message.GetNumberOfNewOrder(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

//// Number of new order with drvider
//  result grt the result here but not pass
router.get('/api/NumberOfNewOrderWitDriver', function (req, res) {
    message.GetNumberOfNewOrderWithDriver(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json([{ 'NumberOfNewOrderWithDriver': Object.keys(result).length }]);
        }
    })
})
router.get('/api/NewOrderWitDriver', function (req, res) {
    message.GetNumberOfNewOrderWithDriver(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

/// All Orders have been delivered (تم تسليمها)
router.get('/api/DeliveredOrders', function (req, res) {
    message.GetAllDeliveredOrders(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})
///////////////////////////////
//get backed orders
router.get('/api/BackedOrders', function (req, res) {
    message.GetBackedOrder(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }

    })
})
router.get('/api/NumberOfBackedOrders', function (req, res) {
    message.GetBackedOrder(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json({ 'NumberOfBackedOrders': Object.keys(result).length });
        }

    })
})
// order in specific date
router.get('/api/orderDate', function (req, res) {
    console.log(req.query)
    data = {
        date: req.query.date
    }
    message.GetOrderInDate(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })

})
//order in from date to date 
router.get('/api/orderDateFromTo', function (req, res) {
    console.log(req.query)
    data = {
        fromDate: req.query.fromDate,
        toDate: req.query.toDate,
    }
    message.GetOrderInDateFromTo(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})


//order in from date to date for specific trader 
router.get('/api/traderOrderDateFromTo', function (req, res) {
    console.log(req.query)
    data = {
        fromDate: req.query.fromDate,
        toDate: req.query.toDate,
        id:req.query.id
    }
    message.GetTraderOrderInDateFromTo(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

//order in from date to date for specific driver  الي عمل عليهم scan
router.get('/api/DriverOrderDateFromTo', function (req, res) {
    console.log(req.query)
    data = {
        fromDate: req.query.fromDate,
        toDate: req.query.toDate,
        id:req.query.id
    }
    message.GetDriverOrderInDateFromTo(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

// all driver info
router.get('/api/allDriver', function (req, res) {
    message.getAllDriver(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

//all trader info 
router.get('/api/allTrader', function (req, res) {
    message.getAllTrader(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

router.get('/api/orderAddingDuringLast48', function (req, res) {
    message.getorderDuringLast48(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})

router.get('/api/orderSubmitDuringLast48', function (req, res) {
    message.getorderSubmitDuringLast48(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400)
            res.end(err + '');
        }
        else {
            res.status(201)
            res.json(result);
        }
    })
})


//////////////////////// Driver //////////////////////////
//get order with driver_id xx 
router.get('/api/orderForDriver', function (req, res) {
    console.log(req.query)
    data = {
        user_id: req.query.IdUser
    }
    message.getAllOrderForDriverNotFinish(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(201);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ 'orders': result });
        }
    })
})

//update order who is the driver  from driver when driver scan the order
router.put('/api/updateOrder_driver', function (req, res) {
    data = req.body;
    // console.log(data)
    data.map((order) => {
        splitOrder = order.barcode.split('-');
        re = { user_id: order.user_id, order_id: splitOrder[1] }

        message.updateOrder_driver(dataBaseInfo, re, function (err, result) {
            if (err) {
                res.status(400);
                res.json([{ "success": false }])
            }
        })
        message.getTraderId(dataBaseInfo, { order_id: splitOrder[1] }, function (error, result) {
            token_not = result[0].token_not;

            var registrationToken = [];
            registrationToken.push(token_not);
            console.log(registrationToken);
            var payload = {
                notification: {
                    title: "حالة الطرد",
                    body: "اصبح الطرد مع سائق جديد",
                    sound: "default",
                },
                data: {
                    NumOfOrders: "" + req.body.length,
                },

            };
            var options = {
                priority: "high",
                timeToLive: 60 * 60 * 24
            };


            admin.messaging().sendToDevice(registrationToken, payload, options)
                .then(function (response) {
                    console.log("Successfully sent message:", response);
                    console.log(response.results[0].error)
                })
                .catch(function (error) {
                    console.log("Error sending message:", error);
                });

        })
    })

    res.status(201);
    res.json([{ "success": true }])
})





module.exports = router