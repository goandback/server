const express=require('express')
const router=express.Router()
var message =require('../messages/cities_message')
var message2=require('../messages/cods_messages')
const dataBaseInfo={
    host:process.env.host,
    user:process.env.user,
    password:process.env.password,
    database:process.env.database,
}

///////////////////////////////// city ///////////////////////////////////////////////

router.post('/api/cities' , function(req , res){
    data ={
        state_id : req.body.state_id ,
        COD_price:req.body.COD_price , 
        city_name : req.body.city_name
    }
    message.postCities(dataBaseInfo , data , function(err , result){
        if(err){
            res.status(201);
            res.json({success :false})
        }
        else{
            if(result.find ){
                res.status(201);
                res.json({success :false , message :result.find })   
            }
            else{
                res.status(201);
                res.json({success :true})
            }
        }
    })
})
router.get('/api/cities' , function(req , res){
    data={
        from_id : req.query.IdFrom ,
        to_id:  req.query.IdTo
    }
    message.getCities(dataBaseInfo , data , function(err , result){
        if(err){
            res.status(400);
            res.end(err+'')
        }
        else{
        message2.getCOD(dataBaseInfo , data , function(error , resul ){
            if(error){
                res.status(400);
                res.end(err+'')
            }
            else{
                var cod=0
                console.log(resul)
                if(Object.keys(resul).length != 0){
                    cod =resul[0].COD_price
                }
                res.status(201);
                res.json({'village' : result , 'cod' :cod})
            }
        })
    }
    })
})
router.get('/api/Allcities' , function(req , res){
    message.getAllCities(dataBaseInfo , function(err,result){
        if(err){
            res.status(400);
            res.end(err+'')
        }
        else{
            res.status(201);
            res.json(result)
        }  
    })
})
router.get('/api/citiesAdmin' , function(req , res){
    data={
        state_id : req.query.state_id,
    }
    message.getCitiesAdmin(dataBaseInfo  , data, function(err,result){
        if(err){
            res.status(400);
            res.end(err+'')
        }
        else{
            res.status(201);
            res.json(result)
        }  
    })
    
})
router.get('/api/SpecificCities' , function(req , res){
    data={
        id:req.query.id ,
    }
    message.getSpecificCityById(dataBaseInfo , data , function(err ,result){
        if(err){
            res.status(400);
            res.end(err+'')
        }
        else{
            res.status(201);
            res.json(result)
        }  
    })
})

router.delete('/api/cities' , function(req , res){
   data={
       id : req.query.id
   } 
   message.deleteCity(dataBaseInfo , data , function(err,result){
    if(err){
        res.status(400);
        res.end(err+'');
    }
    else{
        res.status(201);
        res.json({success :true});   
    }
   })
})
router.put('/api/cities' , function(req , res){
    data ={
        id:req.body.id,
        COD_price:req.body.COD_price , 
        city_name : req.body.city_name
    }
    message.updateCity(dataBaseInfo , data , function(err,result){
     if(err){
         res.status(400);
         res.end(err+'');
     }
     else{
         res.status(201);
         res.json({success :true});   
     }
    })
})




module.exports= router