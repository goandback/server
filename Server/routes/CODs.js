const express = require('express')
const router = express.Router()
var message = require('../messages/CODs_messages')
const dataBaseInfo = {
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
}

///////////////////////////////////// COD 's ///////////////////////////////////////////////

router.post('/api/cod', function (req, res) {
    data = {
        from_id: req.body.from_id,
        to_id: req.body.to_id,
        COD_price: req.body.COD_price
    }
    message.postCod(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '')
        }
        else {
            if (result.find) {
                res.status(201);
                res.json({ success: false, message: result.find })
            }
            else {
                res.status(201);
                res.json({ success: true })
            }
        }
    })
})
router.get('/api/cod', function (req, res) {
    console.log('test' )

    data = {
        from_id: req.query.from_id,
        to_id: req.query.to_id,
    }
    message.getCOD(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '')
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})
router.get('/api/Allcod', function (req, res) {
    message.getAllCOD(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '')
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})
router.get('/api/Allcod2', function (req, res) {
    message.getAllCOD2(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '')
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})
router.delete('/api/cod', function (req, res) {
    data = {
        id: req.query.id
    }
    message.deleteCOD(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '')
        }
        else {
            res.status(201);
            res.json({ success: true })
        }
    })
})
router.put('/api/cod', function (req, res) {
    data = {
        id: req.body.id,
        from_id: req.body.from_id,
        to_id: req.body.to_id,
        COD_price: req.body.COD_price
    }
    message.updateCOD(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ success: true });
        }
    })
})


module.exports = router