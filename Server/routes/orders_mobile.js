const express = require('express')
const router = express.Router()
const message=require('../messages/orders_mobile_messages')
const dataBaseInfo = {
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
}




//update order status
router.put('/api/updateOrderStatus', function (req, res) {
    console.log(req.body)
    data = {
        id: req.body.id,
        status: req.body.status,
        reason: req.body.reason
    }
    message.updateBillStatus(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ success: true });
        }
    })
})

// Finished order for trader 
router.get('/api/DeliveredOrderFoTrader', function (req, res) {
    console.log(req.query)
    data = {
        id: req.query.id,
    }
    message.getDeliveredOrderFoTrader(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})
// backed order for trader 
router.get('/api/BackedOrderFoTrader', function (req, res) {
    console.log(req.query)
    data = {
        id: req.query.id,
    }
    message.getBackedOrderFoTrader(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})

// pending orderes for trader

router.get('/api/PendingOrderFoTrader', function (req, res) {
    console.log(req.query)
    data = {
        id: req.query.id,
    }
    message.getpendingOrderFoTrader(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})


// last month orders for drivee that are delierd 
router.get('/api/driverLastDeliverdOrders', function (req, res) {
    data = {
        id: req.query.id,
    }
    message.getLastmonthDeliverdOrderByDriver(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})
// last month orders for driver that are backed 
router.get('/api/driverLastBackedOrders', function (req, res) {
    data = {
        id: req.query.id,
    }
    message.getLastmonthBackedOrderByDriver(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})

// last month orders for driver that are stucked 
router.get('/api/driverLastStuckOrders', function (req, res) {
    data = {
        id: req.query.id,
    }
    message.getLastmonthStuckOrderByDriver(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.end(err + '');
        }
        else {
            res.status(201);
            res.json({ result:result });
        }
    })
})

module.exports = router