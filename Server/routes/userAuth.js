const express = require('express')
const router = express.Router()
var admin = require('firebase-admin');

var message = require('../messages/userAuth_messages')
const dataBaseInfo = {
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
}

/////////////////////////////// user Auth /////////////////////////

router.post('/api/signUp', function (req, res) {
    date = new Date()
    // var BCRYPT_SALT_ROUNDS = 12;
    // bcrypt.hash(req.body.user_password, BCRYPT_SALT_ROUNDS)
    //.then((HashPassword)=>{

    data = {
        user_Fname: req.body.user_Fname,
        user_Lname: req.body.user_Lname,
        user_password: req.body.user_password,
        user_email: req.body.user_email,
        user_type: req.body.user_type,
        address1: req.body.address1,
        address2: req.body.address2,
        city: req.body.city,
        state: req.body.state,
        postal_code: req.body.postal_code,
        user_phone1: req.body.user_phone1,
        user_phone2: req.body.user_phone2,

        logo: req.body.logo,
        nick: req.body.nick,
        line_no: req.body.line_no,
        GeoLocation: req.body.GeoLocation,

        created_date: date,
        edit_date: date,
    }
    message.checkuser(dataBaseInfo, { user_email: data.user_email }, function (err, result) {
        if (err) {
            res.status(201);
            res.json({ success: false })
        }
        if (Object.keys(result).length == 0) {
            message.createUser(dataBaseInfo, data, function (error, resul) {
                if (error) {
                    res.status(201);
                    res.json({ success: false })
                }
                else {
                    res.status(201);
                    res.json({ success: true })
                }
            })
        }
        else {
            res.status(201);
            res.json({ success: false, message: 'هذا المستخدم مسجل ' })
        }
    })
    //})
})
router.post('/api/signIn', function (req, res) {
    //var token=toAuthtication(req.body.user_email)
    var uid = req.body.user_email
    console.log(req.body)
    admin.auth().createCustomToken(uid)
        .then((token) => {
            var data = {
                user_password: req.body.user_password,
                user_email: req.body.user_email,
                token: token,
            };

            message.SignIn(dataBaseInfo, data, function (err, result) {
                if (err) {
                    res.status(201);
                    res.json({ success: false })
                }
                else {
                    if ((Object.keys(result).length == 1)) {
                        data1 = {
                            token: token,
                            user_id: result[0].user_id,
                        }
                        if (req.body.loginType == 'web' && (result[0].user_type == 'trader' || result[0].user_type == 'driver')) {

                            res.status(201);
                            res.json({ success: false, message: ' خطأ في اسم المستخدم او الباسورد' })
                        }
                        else {

                            if (result[0].isActive == '1') {
                                res.status(201);
                                res.json({ success: false, message: 'خطأ في اسم المستخدم او الباسورد' })
                            }
                            else {
                                message.UpdateUserToken(dataBaseInfo, data1, function (err, resu) {
                                    if (err) {
                                        res.status(201);
                                        res.json({ success: false, message: 'خطأ في اسم المستخدم او الباسورد' })
                                    }
                                    else {
                                        res.status(201);
                                        console.log(result[0].user_id)
                                        res.json({ success: true, user_id: result[0].user_id, token: token, user_type: result[0].user_type })
                                    }
                                })
                            }
                        }
                    }
                    else {
                        res.status(201);
                        res.json({ success: false, message: 'خطأ في اسم المستخدم او الباسورد' })
                    }
                }

            });

        })
    // var BCRYPT_SALT_ROUNDS = 12;
    // bcrypt.hash(req.body.user_password, BCRYPT_SALT_ROUNDS)
    //.then((HashPassword)=>{

    //})

});
router.delete('/api/deleteUser', function (req, res) {
    data = {
        user_id: req.query.user_id
    }
    message.DeleteUser(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(400);
            res.json({ success: false })
        }
        else {
            res.status(201);
            res.json({ success: true })
        }
    })
})
router.get('/api/allUsers', function (req, res) {
    message.GetAllUsers(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400);
            res.json(err + '')
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})

router.put('/api/userTokenNot', function (req, res) {

    data = {
        user_id: req.body.user_id,
        token_not: req.body.token_not
    }
    message.updateNotToken(dataBaseInfo, data, function (error, result) {
        if (error) {
            res.status(400);
            res.end(error + 'err')
        }
        else {
            res.status(201);
            res.json({ success: true })
        }
    })
})
/// get user depending on user Id 
router.get('/api/user', function (req, res) {
    console.log(req.query)
    data = {
        user_id: req.query.user_id
    }
    message.GetUserType(dataBaseInfo, data, function (err, resu) {
        if (err || Object.keys(resu).length == 0) {
            res.status(201);
            res.json({})
        }
        else {

            if (resu[0].user_type == 'driver') {
                message.GetUserDriver(dataBaseInfo, data, function (err, result) {
                    if (err) {
                        res.status(201);
                        res.json({ success: false })
                    }
                    else {
                        console.log(result)
                        res.status(201);
                        res.json(result[0])
                    }
                })
            }
            else if (resu[0].user_type == 'trader') {
                message.GetUserTrader(dataBaseInfo, data, function (err, result) {
                    if (err) {
                        res.status(201);
                        res.json({ success: false })
                    }
                    else {
                        res.status(201);
                        res.json(result[0])
                    }
                })
            }
            else if (resu[0].user_type == 'accountant' || resu[0].user_type == 'admin') {
                res.status(201);
                res.json(resu[0])
            }

        }
    })

})

router.put('/api/updateUser', function (req, res) {
    date = new Date()
    // var BCRYPT_SALT_ROUNDS = 12;
    // bcrypt.hash(req.body.user_password, BCRYPT_SALT_ROUNDS)
    //.then((HashPassword)=>{
    /*   pass=null
       if(req.body.user_password ==null || req.body.user_password =='' ){
           pass=null
       }
       else{
           pass=HashPassword
       }
*/

    console.log(req.body.user_password)
    data = {
        user_id: req.body.user_id,
        user_Fname: req.body.user_Fname,
        user_Lname: req.body.user_Lname,
        user_password: req.body.user_password,
        user_email: req.body.user_email,
        user_type: req.body.user_type,
        address1: req.body.address1,
        address2: req.body.address2,
        city: req.body.city,
        state: req.body.state,
        postal_code: req.body.postal_code,
        user_phone1: req.body.user_phone1,
        user_phone2: req.body.user_phone2,
        isActive: req.body.isActive,

        logo: req.body.logo,
        nick: req.body.nick,
        line_no: req.body.line_no,
        GeoLocation: req.body.GeoLocation,

        edit_date: date,
    }
    message.UpdateUser(dataBaseInfo, data, function (err, result) {
        if (err) {
            res.status(201);
            res.json({ success: false })
        }
        else {
            res.status(201);
            res.json({ success: true })
        }
    });

    //})

})
/// update user password 
router.put('/api/updatePassword', function (req, res) {
    //  var BCRYPT_SALT_ROUNDS = 12;
    // bcrypt.hash(req.body.user_password, BCRYPT_SALT_ROUNDS)
    // .then((HashPassword)=>{
    data = {
        user_id: req.body.user_id,
        user_password: req.body.user_password,
    }
    message.GetUserType(dataBaseInfo, data, function (error, resu) {
        if (error) {
            res.status(201);
            res.json({ success: false, message: 'thier is an error' })
        }
        else if (Object.keys(resu).length == 0) {
            res.status(201);
            res.json({ success: false, message: 'this user is not registeres' })
        }
        else {
            message.UpdateUserPassword(dataBaseInfo, data, function (err, result) {
                if (err) {
                    res.status(201);
                    res.json({ success: false, message: 'thier is an error' })
                }
                else {
                    res.status(201);
                    res.json({ success: true })
                }
            })
        }

    })
    //})

})
/// update is user active 
router.put('/api/isActive', function (req, res) {
    console.log(req.body.isActive)
    data = {
        user_id: req.body.user_id,
        isActive: req.body.isActive
    }
    message.updateUserIsActive(dataBaseInfo, data, function (error, result) {
        if (error) {
            res.status(400);
            res.end(error + '')
        }
        else {
            res.status(201);
            res.json({ success: true });
        }
    })
})
/// get all users not active 
router.get('/api/usersNotActive', function (req, res) {
    message.GetAllUsersNotActive(dataBaseInfo, function (err, result) {
        if (err) {
            res.status(400);
            res.json({})
        }
        else {
            res.status(201);
            res.json(result)
        }
    })
})



module.exports = router