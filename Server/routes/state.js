const express=require('express')
const router=express.Router()
var message =require('../messages/state_messages')
var message1=require('../messages/cities_message')
var message2=require('../messages/cods_messages')
const dataBaseInfo={
    host:process.env.host,
    user:process.env.user,
    password:process.env.password,
    database:process.env.database,
}

///////////////////////////////////// States  ////////////////////////////////////////

router.post('/api/state' ,function(req , res){
    data={
        location:req.body.location,
    }
    message.postState(dataBaseInfo , data , function(err , result){
        if(err){
            res.status(201);
            res.json({success :false})
        }
        else{
            if(result.find ){
                res.status(201);
                res.json({success :false , message :result.find })   
            }
            else{
                id= result.insertId
                data ={
                    state_id : result.insertId ,
                    COD_price: 0 , 
                    city_name : 'وسط البلد '
                }
                message1.postCities(dataBaseInfo , data , function(erro , resul){

                    if(erro){
                        res.status(201);
                        res.json({success :false})
                    }
                    else{
                        data={
                            id:id
                        }
                        message2.getCodForState(dataBaseInfo , data , function(err , result){
                            if(err){
                                res.status(201);
                                res.json({success :false})
                            } 
                            else{
                                res.status(201);
                                res.json(result)
                            }  
                        })
                       
                    }
                })                
            }
        }
    })
})

router.get('/api/state' ,function(req , res){
    console.log('hello')
    message.getStates(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'');
        }
        else{
            res.status(201);
            res.json({'states': result ,'size' : Object.keys(result).length}); 
        }
    })
})

router.get('/api/state1' ,function(req , res){
    console.log('hello')

    message.getStates(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'');
        }
        else{
            res.status(201);
            res.json(result); 
        }
    })

})

router.delete('/api/state' ,function(req , res){
    data={
        id:req.query.id,
    }
    message.deleteState(dataBaseInfo , data , function(err,result){
        if(err){
            res.status(400);
            res.end(err+'');
        }
        else{
            res.status(201);
            res.json({success :true});   
        }
    })
})

router.put('/api/state' ,function(req , res){
    data={
        id:req.body.id , 
        location:req.body.location
    }
    message.updateState(dataBaseInfo , data , function(err , result){
        if(err){
            res.status(400);
            res.end(err+'');
        }
        else{
            res.status(201);
            res.json({success :true});
        }
    })
    
})


module.exports = router