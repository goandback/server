const express=require('express')
const router=express.Router()
var message =require('../messages/packages_messages')
const dataBaseInfo={
    host:process.env.host,
    user:process.env.user,
    password:process.env.password,
    database:process.env.database,
}


////////////////////////////////////// package size  / ////////////////////////////////////////////

router.post('/api/package' , function(req , res ){
    data={
        size : req.body.size ,
        cod : req.body.cod
    }
    message.postPackage(dataBaseInfo , data , function(error , result){
        if(error){
            res.status(400);
            res.end(error+'')  
        }
        else{
            if(result.find ){
                res.status(201);
                res.json({success :false , message :result.find })   
            }
            else{
                res.status(201);
                res.json({success :true})
            }
        }
    })
})
router.get('/api/package' , function(req , res ){
    message.getPackages(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json({'package' : result})
        }
    })  
})
router.get('/api/packageAdmin' , function(req , res ){
    message.getPackages(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json( result)
        }
    })  
})

router.get('/api/packageAdminId' , function(req , res ){
    data={
        id:req.body.id , 
    }
    message.getPackagesById(dataBaseInfo , data, function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json( result)
        }
    })  
})

router.delete('/api/package' , function(req , res ){
    data={
        id:req.query.id
    };
    message.deletePackage(dataBaseInfo , data , function(error , result){
        if(error){
            res.status(400);
            res.end(error+'')  
        }
        else{
            res.status(201);
            res.json({success:true});
        }
    })
})
router.put('/api/package' , function(req , res ){
    data={
        id:req.body.id ,
        size:req.body.size ,
        cod : req.body.cod
    }  
    message.updatePackage(dataBaseInfo , data , function(error , result){
      if(error){
          res.status(400);
          res.end(error+'')  
      }
      else{
          res.status(201);
          res.json({success:true});
      }
    })
})


module.exports = router

