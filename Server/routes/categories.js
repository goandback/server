const express=require('express')
const router=express.Router()
var message =require('../messages/categories_messages')
const dataBaseInfo={
    host:process.env.host,
    user:process.env.user,
    password:process.env.password,
    database:process.env.database,
}
////////////////////////////////////// category ////////////////////////////////////////////

router.post('/api/category' , function(req , res){
    data={
        category:req.body.category
    }
    message.postCategory(dataBaseInfo , data , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            if(result.find ){
                res.status(201);
                res.json({success :false , message :result.find })   
            }
            else{
                res.status(201);
                res.json({success :true})
            }
        }
    })
})
router.get('/api/category' , function(req,res){
    message.getCategories(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json({'category':result})
        }
    })  
})
router.get('/api/categoryAdmin' , function(req,res){
    message.getCategories(dataBaseInfo , function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json(result)
        }
    })  
})
router.get('/api/categoryAdminId' , function(req,res){
    data={
        id:req.body.id
    }
    message.getCategoriesById(dataBaseInfo ,data, function(err, result){
        if(err){
            res.status(400);
            res.end(err+'')  
        }
        else{
            res.status(201);
            res.json(result)
        }
    })  
})

router.delete('/api/category' , function(req , res){
    data={
        id:req.query.id
    };
    message.deleteCategory(dataBaseInfo , data , function(error , result){
        if(error){
            res.status(400);
            res.end(error+'')  
        }
        else{
            res.status(201);
            res.json({success:true});
        }
    })
})
router.put('/api/category' , function(req , res ){
  data={
      id:req.body.id ,
      category:req.body.category
  }  
  message.updateCategory(dataBaseInfo , data , function(error , result){
    if(error){
        res.status(400);
        res.end(error+'')  
    }
    else{
        res.status(201);
        res.json({success:true});
    }
  })
})

module.exports=router